// global variables
var trackSmartResponse = null;
var slackResponse = null;
var userData = {
	working: false,
	totalHours: 0,
	totalClosedConvos: 0,
	clockInTime: null,
	closedThisShift: 0,
	periodStart: null
}

// check for errors in last runtime operation
function checkErrors(){
	if(chrome.runtime.lastError){
		console.log(chrome.runtime.lastError);
	}
}

// set up user data
function syncOnStart(){
	chrome.storage.sync.get({'working': false, 'totalHours': 0, 'totalClosedConvos': 0, 'clockInTime': null, 
		'closedThisShift': 0, 'periodStart': null},
		function(results){
			userData.working = results.working;
			userData.totalHours = results.totalHours;
			userData.totalClosedConvos = results.totalClosedConvos;
			if(results.clockInTime){
				userData.clockInTime = moment(JSON.parse(results.clockInTime));
			}
			userData.closedThisShift = results.closedThisShift;
			if(results.periodStart == null || results.periodStart == undefined){
				// new qp tracking period start date
				userData.periodStart = new Date();
				userData.periodStart = (userData.periodStart.getMonth()+1) +  "/" + userData.periodStart.getDate() + 
					"/" + userData.periodStart.getFullYear();
				// store the new qp start date
				chrome.storage.sync.set({'periodStart': userData.periodStart}, 
					function(){
						checkErrors();
					}
				);
			}else{
				userData.periodStart = results.periodStart;
			}
			checkErrors();
			console.log(results);
		}
	);
}
syncOnStart();

/********************************* ------------------ CLOCK IN/OUT FUNCTIONS -----------------*******************/

// open up the tabs when the user clocks in
function clockIn(){
	chrome.tabs.create({active: false, url: "https://timeclock.tracksmart.com/app/time"},
		function(tab){
			chrome.tabs.executeScript(tab.id,{
				file: "deploy/login.js",
				runAt: "document_end"
			}, function(){
				chrome.tabs.sendMessage(tab.id, {message: "clock-in"});
				checkErrors();
			});
		}
	);

	chrome.tabs.create({active: false, url: "https://codecademy.slack.com/messages/proadvisors/details/"},
		function(tab){
			chrome.tabs.executeScript(tab.id, {file: "libs/moment.min.js"}, function(){
				chrome.tabs.executeScript(tab.id, { file: "libs/jquery-2.1.4.js"}, function(){
					chrome.tabs.executeScript(tab.id, { file: "deploy/slack.js", runAt: "document_end" }, function(){
						chrome.tabs.sendMessage(tab.id, {message: "clock-in"});
						checkErrors();
					});
				})
			});
		}
	);
	chrome.tabs.create({url: "https://app.intercom.io/a/apps/wft4jxth/inbox/unassigned"}, function(){
		checkErrors();
	});

	startQPcounter();
}


function clockOut(){
	// clock out on slack
	chrome.tabs.create({active: false, url: "https://codecademy.slack.com/messages/proadvisors/details/"},
		function(tab){
			chrome.tabs.executeScript(tab.id, {file: "libs/moment.min.js"}, function(){
				chrome.tabs.executeScript(tab.id, { file: "libs/jquery-2.1.4.js"}, function(){
					chrome.tabs.executeScript(tab.id, { file: "deploy/slack.js", runAt: "document_end" }, function(){
						chrome.tabs.sendMessage(tab.id, {message: "clock-out"});
						checkErrors();
					});
				})
			});
		}
	);

	// clock out on tracksmart
	chrome.tabs.create({active: false, url: "https://timeclock.tracksmart.com/app/time"},
		function(tab){
			chrome.tabs.executeScript(tab.id,{
				file: "deploy/login.js",
				runAt: "document_end"
			}, function(){
				chrome.tabs.sendMessage(tab.id, {message: "clock-out"});
				checkErrors();
			});
		}
	);

	stopQPcounter();
}

// start up the QP counter
function startQPcounter(){
	// set the working variable to true
	userData.working = true;
	// clock in time
	userData.clockInTime = moment();
	 // store data in case browser is closed
	var stringTime = JSON.stringify(userData.clockInTime); // cannot store objects in chrome.storage
	chrome.storage.sync.set({'working': true, 'clockInTime': stringTime}, function(){
		checkErrors();
	})
}

// stop the QP counter
function stopQPcounter(){
	// no longer working
	userData.working = false;
	// update the total convos (totalHours is updated by popup.js)
	userData.totalClosedConvos += userData.closedThisShift;
	// store data for later use
	chrome.storage.sync.set({'working': false, 'totalClosedConvos': userData.totalClosedConvos,
		'closedThisShift': 0, 'clockInTime': null, 'totalHours': userData.totalHours},
		function(){
			checkErrors();
		}
	)
	// clean up unused variables
	userData.clockInTime = null;
	userData.closedThisShift = 0;
}

// reset the qp stats
function resetStats(){
	// reset it
	var stringTime = JSON.stringify(userData.clockInTime);
	chrome.storage.sync.set({'working': userData.working, 'totalClosedConvos': 0,
		'closedThisShift': 0, 'clockInTime': stringTime, 'totalHours': 0, 'periodStart': null}, 
		function(){
			checkErrors();
		}
	);
	// sync it back up
	syncOnStart();
}








